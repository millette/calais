#!/usr/bin/env node
/*
# ATTENTION: Sur Debian (par exemple), la commande node
# est implémentée par deux paquets
# https://packages.debian.org/search?searchon=contents&keywords=%2Fbin%2Fnode&mode=path&suite=stable&arch=any
# node offre /usr/sbin/node - un logiciel pour radio-amateurs (sbin pour les superutilisateurs)
# nodejs-legacy offre /usr/bin/node qui est lien symbolique vers /usr/bin/nodejs
# Le paquet nodejs lui-même offre /usr/bin/nodejs qui est l'interpréteur node.js.
# Bref, installer nodejs-legacy si vous êtes sur Debian, mais attendez-vous
# à certains conflits si vous utilisez aussi le paquet pour radio-amateurs.
# Notez cependant que jessie et les versions suivantes ont renommé le paquet pour radio-amateurs
# de node à ax25-node.
*/

var fs = require('fs'),
  path = require('path'),
  CalaisService = require('./calais-lib'),
  config = require('./config.json'),  // Copier config.sample.json dans config.json et adapter au besoin
  cs,
  _options = process.argv.filter(function(v) {return 0 === v.indexOf('--')}),
  options = {},
  _files = process.argv.slice(2).filter(function(v) {return 0 !== v.indexOf('--')}),
  files = {},
  method;

_options.forEach(function(val) {
  var t = val.split('=');
  if ('--help' === val || '-h' === val) {
    console.log('Help');
    console.log('./' + path.basename(process.argv[1]) + ' [--input=input format] [--language=input language] [--output=output format] [inputSource (file or url beginning with http:// or https://) or - or nothing (if no outputFile is given) for stdin] [outputFile or - or nothing for stdout]');
    console.log('Options begin with -- and files don\'t.');
    console.log('./' + path.basename(process.argv[1]) + ' - -');
    console.log('is the same as ./' + path.basename(process.argv[1]) + ' -');
    console.log('is the same as ./' + path.basename(process.argv[1]));
    console.log('Note the use of "-" above to indicate either stdin or stdout depending on position. In all those cases, input source is taken from stdin and the output is sent to stdout.');
    console.log('To specify an output file and use stdin for the input source, use');
    console.log('./' + path.basename(process.argv[1]) + ' - my-output.txt');
    console.log('To specify an input file and use stdout to output, use');
    console.log('./' + path.basename(process.argv[1]) + ' my-input.txt');
    console.log('Available options:');
    console.log('--input= input format, one of text/html, text/xml or text/raw');
    console.log('--language= input language, one of English, French or Spanish');
    console.log('--output= output format, one of xml/rdf, application/json or text/n3');
    console.log('All options are optional and default to values found in config.json. You can put them before or after input and output file names.');
    console.log('About the input source.');
    console.log('As we covered, - (or nothing when no output file is given either) means use stdin to allow piping.');
    console.log('If the input source begins with http:// or https:// it is treated as a URL and will be fetched from the web, unless a file with that particular name exists on the filesystem.');
    process.exit(1);
  }
  options[t[0].slice(2)] = t[1];
});

if (!config.calaisToken) {
  console.log('You must edit your config file (copy config.sample.json to config.json) to include at least your OpenCalais token (key).');
  process.exit(1);
}

_files.forEach(function(val, index, array) {
  files[index?'output':'input'] = val;
});

if ('undefined' === typeof files.input || '-' === files.input) {
  files.input = '-';
  method = 'stdin';
} else {
  if ((0 === files.input.indexOf('http://') || 0 === files.input.indexOf('https://')) && !fs.existsSync(files.input)) {
    files.inputURL = files.input;
    files.input = false;
    method = 'url';
  } else {
    method = 'file';
  }
}

if ('undefined' === typeof files.output) {
  files.output = '-';
}

cs = new CalaisService(
  config.calaisToken,   // Prenez soin d'y mettre votre clé (token) Calais.
  config.inputFormat,   // Si vous mettez false dans la config au lieu d'une valeur par défaut, il faudra obligatoirement passer un input à la méthode
  config.inputLanguage, // Mettez false dans la config pour prendre la valeur par défaut du service OpenCalais (English)
  config.outputFormat   // Mettez false dans la config pour prendre la valeur par défaut du service OpenCalais (text/rdf)
);

if ('stdin' === method) {
  if ('-' === files.output) {
    cs.stdin(options.input||false, options.inputLanguage||false, options.output||false);
  } else {
    cs.stdin(options.input||false, options.inputLanguage||false, options.output||false, function(res) {
      res.pipe(fs.createWriteStream(files.output));
    });
  }
} else {
  if ('-' === files.output) {
    cs[method](files.input||files.inputURL, options.input||false, options.inputLanguage||false, options.output||false);
  } else {
    cs[method](files.input||files.inputURL, options.input||false, options.inputLanguage||false, options.output||false, function(res) {
      res.pipe(fs.createWriteStream(files.output));
    });
  }
}
